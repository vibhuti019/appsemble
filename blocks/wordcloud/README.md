This block is used to display data in a word cloud format. You can specify the data in the fields
parameter. You can add event data objects, a list of strings, or both!
